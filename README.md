# About

This merely my personal website nothing fancy, just a simple corner on the internet where I can express my self freely without the restrictions I may face on social media.
Also the place where I will post about my projects, artworks, writings..etc

## Objectives

I made this website so I can be able to :

1. Stop relying on social media.
1. Share my artworks and ideas.
1. Have fun while helping others(I hope).

## Rules

I made this site upon some rules or criteria :

1. The site must be and remain Free as in freedom.
1. The site's source code must be minimal and with no javascript if possible.
1. The site must contain nothing but important stuff.
1. The site must be my site.


